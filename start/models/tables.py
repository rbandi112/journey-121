# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.

import datetime

def get_user_email():
    return auth.user.email if auth.user else None

db.define_table('user_submissions',
                Field('created_on','datetime', default=request.now),
                Field('created_by', 'reference auth_user', default=auth.user_id),
                Field('submission_url'),
                Field('first_name'),
                Field('last_name'),
                Field('user_email', default=get_user_email()),
                Field('title'),
                Field('descript'),
                Field('submission_check', default = False),
                Field('rating', default = 0),
                Field('finished', default = False)
)



# after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
