// This is the js for the default/index.html view.


var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    // Enumerates an array.
    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};

    self.invert_check = function(_idx){
        $.post(invert_check_url,
            {
                change_id: self.vue.user_submissions[_idx].id
            }, function(data){
            self.get_user_submissions();
            self.get_finished_submissions();
            self.get_draft_submissions();
            })
    }

    self.invert_uncheck = function(_idx){
        $.post(invert_uncheck_url,
            {
                change_id: self.vue.user_submissions[_idx].id
            }, function(data){
            self.get_user_submissions();
            self.get_finished_submissions();
            self.get_draft_submissions();
            })
    }

    self.show_input = function () {
        $("div#uploader_div").show();
        self.vue.is_submitting = true;
    };

    self.hide_input = function () {
        $("div#uploader_div").hide();
        self.vue.is_submitting = false;
        $("input#title_input").val(""); // This clears the file choice once uploaded.
        $("input#descript_input").val(""); // This clears the file choice once uploaded.
        $("input#url_input").val(""); // This clears the file choice once uploaded.
    };

    self.show_draft_input = function () {
        $("div#draft_uploader_div").show();
        self.vue.is_draft = true;
    };

    self.hide_draft_input = function () {
        $("div#draft_uploader_div").hide();
        self.vue.is_draft = false;
        $("input#title_input").val(""); // This clears the file choice once uploaded.
        $("input#descript_input").val(""); // This clears the file choice once uploaded.
        $("input#url_input").val(""); // This clears the file choice once uploaded.
    };

    self.get_user_submissions = function(){
        console.log("HI");
        $.getJSON(get_user_submissions_url,{start_idx: 0, end_idx: 20},
            function (data){
            self.vue.user_submissions = data.user_submissions;
            enumerate(self.vue.user_submissions);
        })
    }

    self.get_current_id = function(){
        $.getJSON(get_current_id_url,{start_idx: 0, end_idx: 20},
            function (data){
            self.vue.current_id = data.current_id;
        })
    }

    self.get_finished_submissions = function(){
        console.log("HI");
        $.getJSON(get_finished_submissions_url,{start_idx: 0, end_idx: 20},
            function (data){
            self.vue.finished_submissions = data.finished_submissions;
            enumerate(self.vue.finished_submissions);
        })
    }

    self.get_draft_submissions = function(){
        console.log("HI");
        $.getJSON(get_draft_submissions_url,{start_idx: 0, end_idx: 20},
            function (data){
            self.vue.draft_submissions = data.draft_submissions;
            enumerate(self.vue.draft_submissions);
        })
    }

    self.upload_submission = function (event) {
        if(self.vue.is_submitting){
            finished="True";
        } else{
            finished = "False";
        }
        setTimeout(function() {
        $.post(upload_submission_url,
            {
                title_input: self.vue.add_title_input,
                descript_input: self.vue.add_descript_input,
                url_input: self.vue.add_url_input,
                finished: finished,
            },
            function(data){
            self.vue.user_submissions.unshift(data.user_submissions);
            enumerate(self.vue.user_submissions);
        });
        }, 1000);
        self.hide_input();
    };
    
    self.set_title = function(_idx){
            $.post(set_title_url,
                {
                    title: self.vue.finished_submissions[_idx].title,
                    _idx: _idx,
                    change_id: self.vue.finished_submissions[_idx].id
                },
                function (data) {
                    new_title= data.title;
                    console.log(new_title);
                    self.get_user_submissions();
                    self.get_finished_submissions();
                    self.get_draft_submissions();
                });
        
    }
    self.set_descript = function(_idx){
            $.post(set_descript_url,
                {
                    descript: self.vue.finished_submissions[_idx].descript,
                    _idx: _idx,
                    change_id: self.vue.finished_submissions[_idx].id
                },
                function (data) {
                    new_descript= data.descript;
                    console.log(new_descript);
                    self.get_user_submissions();
                    self.get_finished_submissions();
                    self.get_draft_submissions();
                });
        
    }
    self.set_url = function(_idx){
        console.log(_idx);
            $.post(set_url_url,
                {
                    url: self.vue.finished_submissions[_idx].submission_url,
                    _idx: _idx,
                    change_id: self.vue.finished_submissions[_idx].id
                },
                function (data) {
                    new_url= data.url;
                    console.log(new_url);
                    self.get_user_submissions();
                    self.get_finished_submissions();
                    self.get_draft_submissions();
                });
        
    }

    self.get_title = function(_idx){
        $.getJSON(get_title_url,{change_id: self.vue.user_submissions[_idx].id,
        },
            function(data){
            current_title = data.title;
            self.get_user_submissions();
            self.get_finished_submissions();
            self.get_draft_submissions();
            });
    }

    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            is_uploading: false,
            self_page: true, // Leave it to true, so initially you are looking at your own images.
            logged_in: false,
            user_email: '',
            is_submitting: false,
            user_submissions:[],
            finished_submissions:[],
            draft_submissions:[],
            current_id: null,
            url_input: null,
            title_input: null,
            descript_input: null,
            add_url_input: null,
            add_title_input: null,
            add_descript_input: null,
            current_title: null,
            fin: false,
            is_draft: false,
            finished: false,
        },
        methods: {
            invert_check: self.invert_check,
            invert_uncheck: self.invert_uncheck,
            upload_submission: self.upload_submission,
            get_user_submissions: self.get_user_submissions,
            get_finished_submissions: self.get_finished_submissions,
            get_draft_submissions: self.get_draft_submissions,
            get_current_id: self.get_current_id,
            set_title: self.set_title,
            set_url: self.set_url,
            set_descript: self.set_descript,
            show_input: self.show_input,
            hide_input: self.hide_input,
            show_draft_input: self.show_draft_input,
            hide_draft_input: self.hide_draft_input,
            get_title: self.get_title,
        }
    });

    self.get_user_submissions();
    self.get_finished_submissions();
    self.get_draft_submissions();
    self.get_current_id();
    $("#vue-div").show();

    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});

