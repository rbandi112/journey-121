import tempfile

# Cloud-safe of uuid, so that many cloned servers do not all use the same uuids.
from gluon.utils import web2py_uuid

def upload_submission():
	submission_id = db.user_submissions.insert(
		title=request.vars.title_input,
		descript = request.vars.descript_input,
		submission_url = request.vars.url_input,
		finished = request.vars.finished
	)
	submit = db.user_submissions(submission_id)
	user_submissions = dict(
		id=submit.id,
		title=request.vars.title_input,
		descript=request.vars.descript_input,
		submission_url=request.vars.url_input,
		rating=0,
		finished = request.vars.finished
	)
	return response.json(dict(
		user_submissions=user_submissions,
	))

@auth.requires_login()
def set_title():
	logger.info("price")
	change_id = request.vars.change_id
	new_title = request.vars.title
	_idx = request.vars._idx
	logger.info(_idx)
	logger.info("price3")
	logger.info(new_title)

	logger.info("Hello1")
	db(db.user_submissions.id == change_id).update(title=new_title)

	return response.json(dict(
		title=new_title
	))

@auth.requires_login()
def set_descript():
	logger.info("price")
	change_id = request.vars.change_id
	new_descript = request.vars.descript
	_idx = request.vars._idx
	logger.info(_idx)
	logger.info("price3")
	logger.info(new_descript)

	logger.info("Hello1")
	db(db.user_submissions.id == change_id).update(descript=new_descript)

	return response.json(dict(
		descript=new_descript
	))

@auth.requires_login()
def set_url():
	change_id = request.vars.change_id
	new_url = request.vars.url
	_idx = request.vars._idx
	logger.info(_idx)
	logger.info(new_url)
	logger.info(change_id)

	db(db.user_submissions.id == change_id).update(submission_url=new_url)
	logger.info("hi")
	return response.json(dict(
		url=new_url
	))

def invert_check():
	change_id = request.vars.change_id
	db(db.user_submissions.id==change_id).update(submission_check=True)
	row = db(db.user_submissions.id == change_id).select()
	prev_rating = db(db.user_submissions.id == change_id).select(db.user_submissions.rating)
	logger.info("1")
	db(db.user_submissions.id == change_id).update(rating=prev_rating)

	db(db.user_submissions.id == change_id).update(rating=prev_rating)

	return response.json(dict(
		submission_check = False
	))

def invert_uncheck():
	change_id = request.vars.change_id
	db(db.user_submissions.id == change_id).update(submission_check=False)
	logger.info("0")
	return response.json(dict(
		submission_check=True
	))

def get_current_id():
	start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
	end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0
	id = 0
	rows = db().select(db.user_submissions.ALL, limitby=(start_idx, end_idx + 1))
	for i, r in enumerate(rows):
		if i < end_idx - start_idx:
			id= id+1

	logger.info("Hello2")
	return response.json(dict(
		current_id=id
	))

def get_user_submissions():
	start_idx = int(request.vars.start_idx) if request.vars.start_idx is not None else 0
	end_idx = int(request.vars.end_idx) if request.vars.end_idx is not None else 0

	user_submissions = []

	if auth.user is not None:
		user_email = auth.user.email

	rows = db().select(db.user_submissions.ALL, limitby=(start_idx, end_idx + 1), orderby=~db.user_submissions.created_by)
	for i, r in enumerate(rows):
		if i < end_idx - start_idx:
			submit = dict(
				id=r.id,
				created_on=r.created_on,
				created_by=r.created_by,
				submission_url=r.submission_url,
				user_email=r.user_email,
				submission_check=r.submission_check,
				title = r.title,
				descript = r.descript,
				rating = r.rating,
				finished= r.finished
			)
			user_submissions.append(submit)

	return response.json(dict(
		user_submissions=user_submissions,
		user_email=user_email
	))

def get_finished_submissions():

	finished_submissions = []

	if auth.user is not None:
		user_email = auth.user.email

	rows = db(db.user_submissions.finished== True).select(orderby=~db.user_submissions.created_by)

	for i, r in enumerate(rows):
		if r.finished:
			submit = dict(
				id=r.id,
				created_on=r.created_on,
				created_by=r.created_by,
				submission_url=r.submission_url,
				user_email=r.user_email,
				submission_check=r.submission_check,
				title=r.title,
				descript=r.descript,
				rating=r.rating,
				finished=r.finished
		)
		finished_submissions.append(submit)

	return response.json(dict(
		finished_submissions=finished_submissions,
		user_email=user_email
	))

def get_draft_submissions():
	draft_submissions = []

	if auth.user is not None:
		user_email = auth.user.email

	rows = db(db.user_submissions.finished== False).select()
	for i, r in enumerate(rows):
		submit = dict(
				id=r.id,
				created_on=r.created_on,
				created_by=r.created_by,
				submission_url=r.submission_url,
				user_email=r.user_email,
				submission_check=r.submission_check,
				title=r.title,
				descript=r.descript,
				rating=r.rating,
				finished=r.finished
		)
		draft_submissions.append(submit)

	return response.json(dict(
		draft_submissions=draft_submissions,
		user_email=user_email
	))

def get_title():
	_idx = request.vars.change_id

	row = db(db.user_submissions.id == _idx).select()

	title = row.title
	return response.json(dict(
		title=title
	))